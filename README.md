# Test setup of the 2nd preCICE reference paper

This repository contains the data as well as the steps to reproduce it used in the data mapping section (2.2) of _Bungartz et al., preCICE 2 – A Sustainable and User-Friendly Coupling Library, 2021_.

These instructions and scripts allow to setup the environment used to record data for various test cases.

These tests use:
* a test function
* a partitioned input mesh with test function evaluated on vertices
* a partitioned output mesh
* an exact data mapping configuration

These tests produce:
* peak memory for participants A and B
* runtime measurements for the mapping preparation and mapping execution times
* numerical error of the mapping result based on the test function

Each test run results in a single sample point.
The complete data-set is then plotted for each output mesh using the input mesh width on the x-axis.

The paper data was generated using the thin-nodes of the [SuperMUC-NG](https://doku.lrz.de/display/PUBLIC/SuperMUC-NG) of Leibniz Rechnenzentrum (LRZ).



## Software used

The majority of the following software was provided by LRZ as modules.
Of the following list, only Boost and PETSc were compiled by hand.

### preCICE

Package | Version
--- | ---
preCICE | [2.2.0](https://github.com/precice/precice/releases/tag/v2.2.0)
GCC | 9.3.0
Intel-MPI | 2019 Update 4
Eigen | 3.3.7
Boost | 1.75.0
PETSc | 3.15.0
LibXML2 | 2.9.7

### ASTE

Package | Version
--- | ---
ASTE | [precice-2nd-ref-paper](https://github.com/precice/aste/tree/precice-2nd-ref-paper)
Python | 3.6 Intel
vtk | 8.1.2
Metis | 5.1.0
GMSH | 4.8.0


## Setting up the test cases

This consists of configuration files and generators.
Both can be found in the ASTE repository.

### Mesh generation

We used the [`wind-turbine-blade.STEP`](https://grabcad.com/library/wind-turbine-blade-25) and scaled it down to fit into the unit cube using `contrib/tools/scale`.

```
./scale wind-turbine-blade.STEP scaled.step
```
Then we used `contrib/tools/mesh` to generate meshes for various element sizes _h_.

```
./mesh scaled.step 0.0005 0.0007 0.001 0.0014 0.002 0.003 0.004 0.006 0.008 0.009 0.01 0.02 0.03
```

Then move them to a known location
```
mkdir -p meshes/original
mv *.vkt meshes/original
```

Then apply the test function to all meshes:
```
cd meshes/original
mkdir -p meshes/evaluated
for m in $( find -name "*.vtk" ); do
  eval_mesh.py $m -o ../evaluated/$( basename $m ) '0.78 + math.cos(10*(x+y+z))'
done;
```

The resulting meshes can be found in `meshes` folder located int this repository.

### Test case generation

The tools and configs can be found in `contrib/mapping-tester/`.

To generate the setups for both test cases, use:
``` 
./generate.py -s setup-turbine-small.json -o testcases/turbine-small/cases
./generate.py -s setup-turbine-big.json -o testcases/turbine-big/cases
``` 

To share the meshes between the test cases, use a link
```
mkdir meshes
mkdir -p meshes/partitioned
ln -s meshes/partitioned testcases/turbine-small/cases/meshes
ln -s meshes/partitioned testcases/turbine-big/cases/meshes
```

Then prepare the meshes:
```
./preparemeshes.py -s setup-turbine-small.json -o testcases/turbine-small/cases
./preparemeshes.py -s setup-turbine-big.json -o testcases/turbine-big/cases
```

Now the test cases are ready and just need a slurm script.

### Slurm script

Modify the slurm scipt in `test.sbatch` to match your HPC system and use it to test the setups.
If everything works proceed to the array script.

### Repeated tests 

Modify the script `array.sbatch` to run multiple cases in an array.
For the configured `5` runs, copy the `testcases/turbine-small/cases` folder to `cases1`,...,`cases5`.
```
cd testcases/turbine-small
for c in $(sed 5); do cp -r cases cases$c ; done

cd testcases/turbine-big
for c in $(sed 5); do cp -r cases cases$c ; done
```

### Processing results

```
cd testcases/turbine-small
for c in $(sed 5); do ./gatherstats.py -o cases$c/ -f turbine-small-$c.csv done

cd testcases/turbine-big
for c in $(sed 5); do ./gatherstats.py -o cases$c/ -f turbine-big-$c.csv done
```

Then aggregate the results using pandas

```py
import pandas

GROUPBY = ["mesh A", "mesh B", "mapping", "constraint"]

def aggregateSmall():
  cases = 5
  df = pandas.concat([
      pandas.read_csv(f"turbine-small-{c}.csv") for c in range(1, cases + 1)
  ])
  df.groupby(GROUPBY).agg("mean").to_csv("turbine-small.csv")

def aggregateBig():
  cases = 5
  df = pandas.concat([
      pandas.read_csv(f"turbine-big-{c}.csv") for c in range(1, cases + 1)
  ])
  df.groupby(GROUPBY).agg("mean").to_csv("turbine-big.csv")


if __name__ == '__main__':
  aggregateSmall()
  aggregateBig()
```

These are the results used to generate the graphs.
