# Evaluated Meshes

This archive contains the meshes used for the test cases.
The test function has already been evaluated on the vertices.
Use paraview to inspect them.
